kerjakan project freecodecamp yang ada di link ini
https://www.freecodecamp.org/learn/responsive-web-design/#responsive-web-design-projects

1. Jelaskan peran kita sebagai front-end web developer/front-end engineer
JAWAB : Front end developer adalah pengembang website yang menggunakan baris kode HTML, CSS, dan JavaScript untuk menghasilkan website dengan tampilan yang menarik. Mereka adalah orang-orang yang mengolah desain murni menjadi website yang interaktif dengan pengguna.

2. hardskill apa saja yang minimal harus dimiliki oleh seorang front-end webd developer/front-end engineer?

 JAWAB : 1. html css javasrcript
         2. virsion control/git
         3. testing
         4.browser developer tools

2. kerjakan project pertama "Build a Tribute Page",
jika sebelum assesment ini diberikan teman-teman sudah menyelesaikan project tersebut
silahkan kerjakan project kedua, jika project kedua juga sudah selesai sebelum
assesment ini diberikan, kerjakan project ketiga dan seterusnya.
untuk sementara silahkan gunakan template codepen yang sudah di sediakan oleh freecodecamp.

- teknis pengumpulan assesment akan dibahas saat materi Git